package luisemilio.org.turismo;

import android.os.Bundle;
import android.support.annotation.IntegerRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import luisemilio.org.turismo.volley.WebService;

public class Hoteles extends AppCompatActivity {
private ListView lstHotels;
private String token;
private ArrayList<HashMap<String, String>> listhot = new ArrayList<HashMap<String, String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hoteles);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lstHotels = (ListView) findViewById(R.id.lstHotels);

        Bundle bundle = getIntent().getExtras();
        token = bundle.getString("token");

        Map<String,String> params = new HashMap<String,String>();
        params.put("token",token);

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
                WebService.getHoteles+"?token="+token.toString(),
                new JSONObject(params),
                new Response.Listener<JSONArray>(){
                    @Override
                    public void onResponse(JSONArray response){
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject h = response.getJSONObject(i);
                                HashMap<String, String> map = new HashMap<String, String>();

                                map.put("nombre", h.getString("nombre"));
                                map.put("direccion", h.getString("direccion"));
                                map.put("web",h.getString("web"));
                                map.put("telefono", h.getString("telefono"));
                                map.put("estrellas", h.getString("estrellas"));
                                map.put("piscina", h.getString("piscina"));
                                map.put("wifi", h.getString("wifi"));
                                map.put("mascotas",h.getString("mascotas"));
                                map.put("bar", h.getString("bar"));
                                map.put("idDepartamento", h.getString("idDepartamento"));

                                listhot.add(map);
                            };
                            ListAdapter adapter = new SimpleAdapter(Hoteles.this, listhot,
                                    R.layout.list_h,
                                    new String[] { "nombre","direccion", "web","telefono",
                                            "estrellas","piscina","wifi","mascotas","bar",
                                            "idDepartamento" }, new int[] {
                                    R.id.nombre,R.id.direccion, R.id.web, R.id.telefono,
                                    R.id.estrellas,R.id.piscina,R.id.wifi,R.id.mascotas,
                                    R.id.bar,R.id.idDepartamento
                            });

                            lstHotels.setAdapter(adapter);
                            lstHotels.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent, View view,
                                                        int position, long id) {
                                    Toast.makeText(Hoteles.this, "You Clicked at "+
                                            listhot.get(+position).get("nombre"), Toast.LENGTH_SHORT).show();

                                }
                            });
                        } catch (JSONException j){
                            Log.d("Error: Response ", j.getMessage());
                        }
                    }

                },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error: Response ", error.getMessage());
            }

        });

        WebService.getInstance(Hoteles.this).addToRequestQueue(request);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
            }
        });
    }

}
