package luisemilio.org.turismo;

import android.app.DownloadManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import luisemilio.org.turismo.beans.Usuario;
import luisemilio.org.turismo.volley.WebService;

public class Login extends AppCompatActivity {
    private TextView txtEmail,txtPassword;
    private Button btnLogin,btnRegistro;
    private Usuario usuarioLoggeado = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegistro = (Button) findViewById(R.id.btnRegistro);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtPassword = (TextView) findViewById(R.id.txtPassword);
        btnLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Map<String,String> params = new HashMap<String,String>();
                params.put("correo",txtEmail.getText().toString());
                params.put("contrasenia",txtPassword.getText().toString());

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                        WebService.autenticar,
                        new JSONObject(params), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject usuario = response.getJSONObject("user");
                            if (usuario.length()>0) {
                                //Toast.makeText(getApplicationContext(), "Bienvenido", Toast.LENGTH_LONG).show();
                                usuarioLoggeado = new Usuario(usuario.getInt("idUsuario"),
                                        usuario.getInt("idRol"),
                                        usuario.getString("nombre"),
                                        usuario.getString("correo"),
                                        usuario.getString("nick"),
                                        "",
                                        response.getString("token"),
                                        response.getString("exp")
                                );
                                Intent intent = new Intent(Login.this, Hoteles.class);
                                intent.putExtra("token", response.getString("token"));
                                startActivity(intent);
                            } else {
                                Toast.makeText(getApplicationContext(),"Verifique sus Credenciales", Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception x) {
                            Log.e("Respone exception ", x.getMessage());
                            //Toast.makeText(getApplicationContext(), x.getMessage(), Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error: Response ", error.getMessage());
                    }
                });

                WebService.getInstance(v.getContext()).addToRequestQueue(request);

            }


        });
        btnRegistro.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startActivity(new Intent(Login.this, Registro.class));
            }


        });

    }
}
