package luisemilio.org.turismo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import luisemilio.org.turismo.beans.Usuario;
import luisemilio.org.turismo.volley.WebService;

public class Registro extends AppCompatActivity {
    private TextView txtNombre,txtEmail,txtPassword,txtNickname;
    private Button btnRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        txtNombre = (TextView) findViewById(R.id.txtNombre);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtPassword = (TextView) findViewById(R.id.txtPassword);
        txtNickname = (TextView) findViewById(R.id.txtNickname);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);
        btnRegistrar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Map<String,String> params = new HashMap<String,String>();
                params.put("nombre",txtNombre.getText().toString());
                params.put("correo",txtEmail.getText().toString());
                params.put("nick",txtNickname.getText().toString());
                params.put("contrasenia",txtPassword.getText().toString());
                params.put("idRol","1");

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                        WebService.registrar,
                        new JSONObject(params), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //JSONObject usuario = response.getJSONObject("user");
                            if (response.length()>0) {
                                Toast.makeText(getApplicationContext(), "Usuario agregado correctamente", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(Registro.this, Login.class));
                            } else {
                                Toast.makeText(getApplicationContext(),"Verifique sus Credenciales", Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception x) {
                            Log.e("Respone exception ", x.getMessage());
                            Toast.makeText(getApplicationContext(), x.getMessage(), Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error: Response ", error.getMessage());
                    }
                });

                WebService.getInstance(v.getContext()).addToRequestQueue(request);

            }


        });

    }
}
