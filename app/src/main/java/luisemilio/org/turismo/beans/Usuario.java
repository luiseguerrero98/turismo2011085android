package luisemilio.org.turismo.beans;

/**
 * Created by Luis on 25/05/2016.
 */

public class Usuario {
    public Usuario() {
    }

    public Usuario(Integer idUsuario, Integer idRol, String nombre, String correo, String nick, String contrasenia, String token, String exp) {
        this.idUsuario = idUsuario;
        this.idRol = idRol;
        this.nombre = nombre;
        this.correo = correo;
        this.nick = nick;
        this.contrasenia = contrasenia;
        this.token = token;
        this.exp = exp;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    private Integer idUsuario;
    private Integer idRol;
    private String nombre;
    private String correo;
    private String nick;
    private String contrasenia;
    private String token;
    private String exp;

}
